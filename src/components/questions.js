export default [
  {
    question: { label: 'Жар', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['COVID-19', 'Грипп'], icon: '' },
      { text: 'Редко', in: ['Простуда'], icon: '' },
      { text: 'Иногда', in: ['Аллергия'], icon: '' }
    ]
  },
  {
    question: { label: 'Сухой кашель', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['COVID-19', 'Грипп'], icon: '' },
      { text: 'Умеренно', in: ['Простуда'], icon: '' },
      { text: 'Иногда', in: ['Аллергия'], icon: '' }
    ]
  },
  {
    question: { label: 'Затрудненное дыхание', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['COVID-19', 'Аллергия'], icon: '' },
      { text: 'Нет', in: ['Простуда', 'Грипп'], icon: '' }
    ]
  },
  {
    question: { label: 'Головная боль', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Иногда', in: ['COVID-19', 'Аллергия'], icon: '' },
      { text: 'Часто', in: ['Грипп'], icon: '' },
      { text: 'Редко', in: ['Простуда'], icon: '' }
    ]
  },
  {
    question: { label: 'Боли в мышцах', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['Простуда', 'Грипп'], icon: '' },
      { text: 'Иногда', in: ['COVID-19'], icon: '' },
      { text: 'Нет', in: ['Аллергия'], icon: '' }
    ]
  },
  {
    question: { label: 'Больное горло', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['Простуда', 'Грипп'], icon: '' },
      { text: 'Иногда', in: ['COVID-19'], icon: '' },
      { text: 'Нет', in: ['Аллергия'], icon: '' }
    ]
  },
  {
    question: { label: 'Утомление', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Часто', in: ['Грипп'], icon: '' },
      { text: 'Иногда', in: ['Простуда', 'COVID-19', 'Аллергия'], icon: '' }
    ]
  },
  {
    question: { label: 'Диарея', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Редко', in: ['COVID-19'], icon: '' },
      { text: 'Нет', in: ['Простуда', 'Аллергия'], icon: '' },
      { text: 'Иногда*', in: ['Грипп'], icon: '' }
    ]
  },
  {
    question: { label: 'Насморк', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Редко', in: ['COVID-19'], icon: '' },
      { text: 'Часто', in: ['Простуда', 'Аллергия'], icon: '' },
      { text: 'Иногда', in: ['Грипп'], icon: '' }
    ]
  },
  {
    question: { label: 'Чихание', text: '', pic: '', thumb: '' },
    answers: [
      { text: 'Нет', in: ['COVID-19', 'Грипп'], icon: '' },
      { text: 'Часто', in: ['Простуда', 'Аллергия'], icon: '' }
    ]
  }
]
